package com.enjoyor.frameforenjoyor.protocol;

import com.enjoyor.frameforenjoyor.model.BaseModel.BaseResponse;
import com.enjoyor.frameforenjoyor.model.MineModel;

import java.util.List;

import retrofit.http.GET;
import rx.Observable;

/**
 * Created by Administrator on 2016/6/29.
 */
public interface MineProtocol{
        @GET("/record/notes/947.action")
        Observable<BaseResponse<List<MineModel>>> getMine();
}
