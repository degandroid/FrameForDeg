package com.enjoyor.frameforenjoyor;

import android.app.Application;

import com.enjoyor.frameforenjoyor.db.DBHelper;

/**
 * Created by Administrator on 2016/6/29.
 */
public class EnjoyorApplication extends Application{
    private static EnjoyorApplication enjoyorApplication;
    private DBHelper mDBHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        enjoyorApplication = this;
    }


    public static EnjoyorApplication getInstance() {
        return enjoyorApplication;
    }

    public DBHelper getDBHelper() {
        if (mDBHelper == null) {
            mDBHelper = new DBHelper(this);
        }
        return mDBHelper;
    }



}
